FROM fedora:32

RUN echo "install_weak_deps=false" >> /etc/dnf/dnf.conf && \
    dnf -y install buildah podman runc && \
    dnf clean all
